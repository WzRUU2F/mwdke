**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.
Bwvh7SGCR5MuHPgXZY4v7WNBLn4egnADNnqJzfDJumeeXM0f0o2MmyuxzleKsq5fUr4eQuTFrwDW1TIFoJcFhZ5y7a1a6JS8V7wrEStgL8c4Kag2IwSB3haN4LCOnvQ7rwQQPoS5zeAyJMgJdFUIy4815xajcZWX48QoXkbw61uxGPNZWFtKFf4Tt702sRMOl07Okx3eocdV6p1qMAsVzDMWSDz4sf5CtwGZFdIeVPT56V5t7qzTEK4UD8QQI1m4hZlfhbUqKw7ELB72fy4N7viF8b1TQnliCQnShZDE4VzLro8l76ONGqFdfvaIYUM5xZKyeIORzAKuFfRoCB44Bv9UFcYu2vKrg8BtkaixGOd6VyoUOVT7h3EV7o57b7VDx3K01TfJpM9Mqp4lLqYuaSm890QawR5h7P34YIJKqoCz9D76SfhhNkY5HNjYIL2ukNiwYTccw7UYcmiNF4HqxWVmgEBdPHsrVZpbmzvztyu19foPuQg41MtkyU14N24lRoz2s2fY30YBJuxKSx8XwLKNIJAadOvbjNo8yI65sUeDhVWGjQ0WzRUU2FsdGVkX181D237kPZXinP7VzqnFOUeTYFzUyu6v0F3EocZjfwRp4hkf1y8x63XztuZJPBvk6eiTVoGYo5ZESBeHWN0PP6oHO34IJ9z1oPAYucs1NFVuB2C/rm6nHFouwFMdk95/J3gjWcz7jJYhC+oBdO9+CMB8lmG3BuoQ663bCAkC3gTqEoZiuMBsXIDU1k8KrGTgRtQyN6hIbYKRpQVb1gR/ceglMvVaat2Hr3GurDp3IMRqIJv017hM5wB1i+7tXK+H5b4YJx5HNQWYxJ2uSyoe2tuRzJctIrc5FLLCCjFJYVYaqzKIpZ6rL0K+KVuEMC5oTsrZVHHLwDa+TmpeIUPLwcWUGOQI/Ig6qNn5lHsdFqOuU2L3NLkfZJgSV2I2aMvd5lFJYG5fpj6h4w3ynYa64C84YJqRgbVVimu+ZlsfSFwYV1wc2e/c3puM+b3am+mjx63edB/TZtS4JGrkbga0mC+6DSthDJ2Mmxc7LE80uS4DB8KZ5+ahcmhfxr5I1xcEtbueCK8yVUtmK/ouN03pF9RNCUclggemYriYwvGmRJXr/az4sDaqGpoRmxZH8jeUgv+b60/xN1XoCNQIGuK8o6RIjHMcMfJU2Y=K8du9IlFDoFUhzXXTv4CXl83aQGsfFKBL9OHIWN8tKJsBlhisKSncA8s7rvRhpLmBg1wMrHMuEKcwZ06dbegWr2YTD3DQ5HiCTalmsOu4sz1fkJl7gb0PQgLaC7Og42Kjd4jBJo6Ei3RGJzdVwm8cWbkkZavfzD3FiCsvA6nWH36vqTUXnWyqtbo57YRacqfkndqbaxODYFw0WvZ0lQNxg6ReYmYeOHFZZFpYDx2z9ubNaSNqjm1rTXgOyaynMFnR3lqIZK84PIhLL4QLwJMCgmXxFyRHt8XLE5NsSGsYUABdFizZdjVnWKPm6K1l4fL0ZwoRjh2sx3lrecO8EptV7I6MwDkeEjyZvmdE34MIXZY0LzRZRuckAdiLqguZSYkNE3b9JeC8Jd7Kz1gPKRbTwRb67bWn6MLbsL9q5wJkZGOCqOIFKNSW7G0qGbuTxeOmt1pLQE9N9Hwq6RZsgoord6JhWbK23Q7JHb38btquyw3jOoiTfmr6J4xqaRR6DG4pSVbog5rAzVPKBIwgcOC7ymoEqy8MtoBt02dWTF3SvDjc6nnA9WDkpEi
---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).